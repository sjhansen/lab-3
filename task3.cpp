/**
 * @file	task3.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */

#include "task3.h"
#include "segmentDisplay.h"
#include "srriSched.h"


void Task3Setup() {
    SegmentDisplaySetup();
}

/***************************************************
 * @brief Make the 7-Segment LED display count up by 
 *        1 unit every 100 ms.
 *
 */
void Task3() {
    
    static int time3 = 0;
    time3++; 
    
    int tmp = time3 / 5;
    int ones = tmp % 10;
    tmp = tmp / 10;
    int  tens = tmp % 10;
    tmp = tmp / 10;
    int  hundreds = tmp % 10;
    tmp = tmp / 10;
    int thousands = tmp % 10; 

    ClearDisplay();
	SelectDigit(1);
	WriteDigit(thousands);
	delay(5);

	ClearDisplay();
	SelectDigit(2);
	WriteDigit(hundreds);
	delay(5);

	ClearDisplay();
	SelectDigit(3);
	WriteDigit(tens);
	delay(5);

	ClearDisplay();
	SelectDigit(4);
	WriteDigit(ones);
	delay(5);

    if (time3 == 9999) {
        time3 = 0;
    }
}

/***************************************************
 * @brief Make the 7-Segment LED display count up by 
 *        1 unit every 100 ms using sleep_474
 *
 */
void Task3Sleepy() {
    static int time3 = 0;
    static int curDig = 1;
    static int currentTargetNumber = 0;
    time3 += 2;   // 2ms has passed
    
    if (time3 >= TIMER_REFRESH_TIME) {
        currentTargetNumber++;
        time3 = 0;
    }

    int tmp = currentTargetNumber;
    int ones = tmp % 10;
    tmp = tmp / 10;
    int  tens = tmp % 10;
    tmp = tmp / 10;
    int  hundreds = tmp % 10;
    tmp = tmp / 10;
    int thousands = tmp % 10; 

    switch(curDig) {
		case 1:
			ClearDisplay();
            SelectDigit(1);
            WriteDigit(thousands);
            //sleep_474(5);
			break;
		case 2:
			ClearDisplay();
            SelectDigit(2);
            WriteDigit(hundreds);
            //sleep_474(5);
			break;
		case 3:
			ClearDisplay();
            SelectDigit(3);
            WriteDigit(tens);
            //sleep_474(5);
			break;
		case 4:
			ClearDisplay();
            SelectDigit(4);
            WriteDigit(ones);
            //sleep_474(5);
			break;
	}
    curDig++;

    if (curDig > 4) {
        curDig = 1;
    }

    if (currentTargetNumber >= DIGIT_MAX_VALUE) {
        currentTargetNumber = 0;
    }
}