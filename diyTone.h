/**diyTone.h
 * @file	diyTone.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */

#include <Arduino.h>


/***************************************************
 * @brief Perform necessary setup to use DiyTone
 *
 *      Prepare the speaker to play tones.
 *      
 */
void ToneSetup();


/***************************************************
 * @brief Play a given tone on pin 6
 *
 *      @param freq The frequency in MHz to play
 *
 *      Play a tone to a speaker defined on pin 6 using
 *      timer 4. Tone will continue to play until diyNoTone()
 *      is called.
 *      
 */
void diyTone(uint16_t freq);

/***************************************************
 * @brief Stop any tone that may currently be playing
 *
 *      Stops any tone that may currently be playing
 *      on pin 6.
 *      
 */
void diyNoTone();
