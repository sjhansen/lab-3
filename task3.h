/**
 * @file	task3.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */
#define TIMER_REFRESH_TIME 100
 #define DIGIT_MAX_VALUE 9999

/**
 * @brief Perform necessary setup for task 3.
 * 
 */
void Task3Setup();

/***************************************************
 * @brief Make the 7-Segment LED display count up by 
 *        1 unit every 100 ms.
 *
 */
void Task3();

/***************************************************
 * @brief Make the 7-Segment LED display count up by 
 *        1 unit every 100 ms using sleep_474
 *
 */
void Task3Sleepy();