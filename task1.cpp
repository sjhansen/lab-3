/**
 * @file  task1.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>
#include "task1.h"
#include "srriSched.h"

volatile int LED_ON = 0;

void Task1Setup() {
  pinMode(LED_PIN, OUTPUT);
}

void Task1() {
  digitalWrite(LED_PIN, HIGH);
  delay(250);
  digitalWrite(LED_PIN, LOW);
}

void Task1Sleepy() {
  if (LED_ON) {
    digitalWrite(LED_PIN, LOW);
    sleep_474(750);
  } else {
    digitalWrite(LED_PIN, HIGH);
    sleep_474(250);
  }
  LED_ON = !LED_ON;
}