/**
 * @file  diyTone.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>
#include "diyTone.h"


void ToneSetup() {

  // we are using timer 4
  TIMSK4 = 0;   // disable interrupts
  TCCR4A = 0;   // normal counting mode
  TCCR4B = 0;   // timer mode
  OCR4A = 0;    // not generating any freq

  // set toggle bit
  TCCR4A |= (1 << COM4A0);

  // Select clock source (no prescaling, N = 1)
  TCCR4B |= (1 << CS40);

  // Set timer to CTC mode
  TCCR4B |= (1 << WGM42);

  // Set PH3 (~D6) to output.
  DDRH |= (1 << 3);
}

void diyTone(uint16_t freq) {
  if (freq > 0) {
    // Note: f_OC4A = f_{clk_I/O} /(2*N*(f_{OCR4A + 1)) (pg. 121), N = 1
    OCR4A = (float) F_CPU / (2 * freq) - 1;
  } else {
    OCR4A = 0;
  }
}

void diyNoTone() {
  OCR4A = 0; // not generating any freq
}