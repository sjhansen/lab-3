/**
 * @file  rrSched.h
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

/***************************************************
 * @brief Manages tasks 1 and 2 on a 1 second period
 * 
 *      Manages multiple tasks with set durations by
 *      calling tasks in order and then delaying until
 *      the period is complete.
 *
 */
void RRSched();