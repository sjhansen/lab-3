/**
 * @file	task2.h
 *		@author		Ava Cole
 * 		@author		Sarah Hansen
 * 		@date			25-Febuary
 * 		@brief		...
 * 
 * 		detailed description ...
 */

#define NOTE_D4 293
#define NOTE_E4 329
#define NOTE_C4 261
#define NOTE_C3 130
#define NOTE_G3 196
#define NOTE_R  0

/***************************************************
 * @brief Play a melody on the speaker
 *
 *      Make the speaker emit the theme from “Close 
 * 			Encounters of the Third Kind”. Call every 1
 *      second.
 * 
 */
void Task2();

/***************************************************
 * @brief Play a melody on the speaker using sleep_474
 *
 *      Make the speaker emit the theme from “Close 
 *      Encounters of the Third Kind”.
 * 
 */
void Task2Sleepy();