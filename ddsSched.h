/**
 * @file  ddsSched.h
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include <Arduino.h>

// the max number of tasks our scheduler can handle, INCLUDING schedule_sync
#define NTASKS 10

// Possible states a task can be in.
typedef enum {
    READY, 
    RUNNING, 
    SLEEPING, 
    DEAD
} STATE;

// Possible states the sFlag variable can be in,
// either waiting for an interrupt to sync to (pending)
// or ready to sync to the latest interrupt (done)
typedef enum {
    PENDING,
    DONE
} SFLAG_STATE;

// The task control block
/**
 * @brief Struct to hold all of a Tasks running state
 * 
 */
typedef struct {
    void (*fptr)();             // the function pointer
    unsigned short int state;   // the task state
    unsigned int delay;         // sleep delay
    unsigned int runTime;       // current run time for the given task
    unsigned int id;            // unique task ID
    char name[21];              // string task name (up to 20 characters)
    unsigned int timesRun;      // total # of times started
} TCB;

/***************************************************
 * @brief remove task from schedule queue for t milliseconds
 * 
 * 			@param t Number of milliseconds to sleep for.
 * 
 * 			The task this function is being called from
 * 			will not be scheduled for the next t milliseconds.
 * 
 * 			@warning t is only accurate to 2 milliseconds
 *
 */
void sleep_474(int t);

/***************************************************
 * @brief sync da scheduler
 * 
 * 			Wait for an interrupt to update sFlag then updates
 * 			timers for all sleeping tasks and changes state to
 * 			READY for all tasks that are done sleeping.
 *
 */
void schedule_sync();

/**
 * @brief sets task to DEAD
 * 
 *          The state for the task this function is called from
 *          will be set to DEAD and the task will be moved to
 *          the dead tasks list.
 * 
 */
void task_self_quit();

/**
 * @brief sets task to READY
 * 
 *          @param tsk pointer to the TCB of a DEAD task
 * 
 *          The state for the task this function is called from
 *          will be set to READY and the task will be moved to
 *          the ready tasks list.
 * 
 */
void task_start(TCB *tsk);

/***************************************************
 * @brief schedule queue of function pointers
 * 
 * 			@param taskQueue An array containing task
 * 				functions to be scheduled. The last task
 * 				must be scheduler_sync. The last task
 * 				must be followed by NULL.
 *          @param names An array containing corresponding
 *          task names up to 20 characters.
 * 
 * 			This scheduler will schedule each task in the
 * 			taskQueue every 2 milliseconds as long as the
 * 			combined duration of all the tasks does not
 * 			exceed 2 milliseconds.
 *
 */
void DDSSched(void (*taskQueue[NTASKS])(), String names[]);