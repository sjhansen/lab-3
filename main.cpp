/**
 * @file  main.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#define DEMO THREE

#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5
#define SIX 6

#include <Arduino.h>
#include <Wire.h>
//#include <Serial.h>

#include "task1.h"
#include "task2.h"
#include "task3.h"
#include "segmentDisplay.h"
#include "diyTone.h"
#include "rrSched.h"
#include "srriSched.h"
#include "ddsSched.h"

void setup() {
  Task1Setup();
  ToneSetup();
  Task3Setup();
}

void loop() {
  // demo 1
  #if DEMO == ONE
  RRSched();
  
  // demo 2
  #elif DEMO == TWO
  void (*Demo2taskQueue[NTASKS]) () = {
    Task1Sleepy,
    Task2Sleepy,
    schedule_sync,
    NULL
  };
  SRRISched(Demo2taskQueue);

  // demo 3
   #elif DEMO == THREE
  void (*Demo3taskQueue[NTASKS]) () = {
    Task1Sleepy,
    Task2Sleepy,
    schedule_sync,
    NULL
  };
  String names[] = {"LED Flasher", "Music Player", "DISPLAY"};
  DDSSched(Demo3taskQueue, names);

  // demo 4
  #elif DEMO == FOUR
   //Task3();
  void (*Demo4taskQueue[NTASKS])() = {
    Task1Sleepy,
    //Task2Sleepy,
    Task3Sleepy,
    schedule_sync,
    NULL,
  };
  SRRISched(Demo4taskQueue); 

  // demo 5
  #elif DEMO == FIVE
  /* TODO: TCB list or something {
    Task4
  }
  DDSched();*/

  // demo 6
  #elif DEMO == SIX
  /* TODO: TCB list or someting {
    Task5
  }
  DDSched();*/
  #endif
}


