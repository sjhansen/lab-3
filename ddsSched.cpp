/**
 * @file  ddsSched.cpp
 *    @author   Ava Cole
 *    @author   Sarah Hansen
 *    @date     25-Febuary
 *    @brief    ...
 * 
 *    detailed description ...
 */

#include "ddsSched.h"

// TODO: how to handle string names???
String NAMES[NTASKS];

// We initialize the state of the scheduler to be pending
// an interrupt to sync to.
volatile SFLAG_STATE sflag = PENDING;

// Lists of tasks (they function more like Maps)
TCB taskList[NTASKS];
// TCB deadTasks[NTASKS];
void (*deadTasks[NTASKS])() = {NULL};

unsigned int curIndex = 0; // current task index

void sleep_474(int t) {
    taskList[curIndex].state = SLEEPING;
    taskList[curIndex].delay = t;
}

/***************************************************
 * @brief sync da scheduler
 * 
 * 			Wait for an interrupt to update sFlag then updates
 * 			timers for all sleeping tasks and changes state to
 * 			READY for all tasks that are done sleeping.
 *
 */
void schedule_sync() {
	// Wait for interrupt.
	while(sflag == PENDING);
	// decriment all sleep times by 2ms
	for (int i = 0; i < NTASKS; i++) {
        if (taskList[i].fptr != NULL) {
            if (taskList[i].state == SLEEPING) {
                // wake up any sleeping tasks whose sleep time is 0
                if (taskList[i].delay < 2) {
                    taskList[i].state = READY;
                } else {
                    taskList[i].delay -= 2;
                }
            }
        }
        taskList[i].runTime += 2;
	}
	// we need another interrupt before we can sync again!
	sflag = PENDING;
}

// /***************************************************
//  * @brief update the state of the program on interrupt
//  * 
//  * 			When an interrupt is recieved from the timer,
//  * 			update sFlag so that our scheduler can syncronize
//  * 			with the interrupt.
//  *
//  */
ISR(TIMER0_COMPA_vect) {
	sflag = DONE;
}

/**
 * @brief Setup the timer w/interrupt
 * 
 */
void timerSetup() {
    // Setup timer for interrupt. We are using timer 0.
	cli(); // disable interupts
    //TIMSK0 = 0;   // disable interrupts
    TCCR0A = 0;   // normal counting mode
    TCCR0B = 0;   // timer mode
    TCNT0  = 0;   //initialize counter value to 0
    // set prescaller to 256
    TCCR0B |= (1 << CS02); 
        // Note: OCR = [F_CPU /(prescaler * freq)]-1
    // we want intterupts every 2ms so freq = 500hz
    OCR0A = 124; //(float) F_CPU / (256 * 500) - 1;
    // Set timer to CTC mode
    TCCR0A |= (1 << WGM01);
    // enable timer compare interrupt
    TIMSK0 |= (1 << OCIE0A);
	sei(); // reenable interuppts
}

void task_start(TCB *tsk) {
    taskList[curIndex].fptr = deadTasks[curIndex];
    deadTasks[curIndex] = NULL;
    taskList[curIndex].state = READY;
}

void task_self_quit() {
    taskList[curIndex].state = DEAD;
    deadTasks[curIndex]  = taskList[curIndex].fptr;
    taskList[curIndex].fptr = NULL;
}

void DDSSched(void (*tasks[NTASKS])(), String names[]) {
    timerSetup();
	// Set up TCBs for each of the given tasks
    // and initialize all spots in task list to NULL
    for (int i = 0; i < NTASKS; i++) {
        if (tasks[i] != NULL) {
            NAMES[i] = names[i];
            strcpy(taskList[i].name, NAMES[i].c_str());
            taskList[i].fptr = tasks[i];
            taskList[i].id = i;
            taskList[i].timesRun = 0;
            taskList[i].runTime = 0;
            taskList[i].delay = 0;
            taskList[i].state = READY;
        } else {
            taskList[i].fptr = NULL;
        }
    }

    // Scheduler - run through each task
    while (true) {
        for (int i = 0; i < NTASKS; i++) {
            if (taskList[i].fptr != NULL && taskList[i].state == READY) {
                taskList[i].state = RUNNING;
                curIndex = i;
                taskList[i].fptr();
                taskList[i].timesRun++;
                if (taskList[i].state == RUNNING) {
                    taskList[i].state = READY;
                }
            }
        }
    }
}
